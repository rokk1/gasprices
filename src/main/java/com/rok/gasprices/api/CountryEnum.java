package com.rok.gasprices.api;

public enum CountryEnum {
    AL("ALBANIA", "al", "Albanija"),
    AD("ANDORRA", "ad", "Andora"),
    AT("AUSTRIA", "at", "Avstrija"),
    BE("BELGIUM", "be", "Belgija"),
    BY("BELARUS", "by", "Belorusija"),
    BA("BOSNIA AND HERZEGOVINA", "ba", "BIH"),
    BG("BULGARIA", "bg", "Bolgarija"),
    CZ("CZECH REPUBLIC", "cz", "Češka"),
    ME("MONTENEGRO", "me", "Črna gora"),
    DK("DENMARK", "dk", "Danska"),
    EE("ESTONIA", "ee", "Estonija"),
    FI("FINLAND", "fi", "Finska"),
    FR("FRANCE", "fr", "Francija"),
    GR("GREECE", "gr", "Grčija"),
    HR("CROATIA", "hr", "Hrvaška"),
    IE("IRELAND", "ie", "Irska"),
    IS("ICELAND", "is", "Islandija"),
    IT("ITALY", "it", "Italija"),
    LV("LATVIA", "lv", "Latvija"),
    LI("LIECHTENSTEIN", "li", "Liechtenstein"),
    LT("LITHUANIA", "lt", "Litva"),
    LU("LUXEMBOURG", "lu", "Luksemburg"),
    HU("HUNGARY", "hu", "Madžarska"),
    MD("MOLDOVA", "md", "Moldavija"),
    DE("GERMANY", "de", "Nemčija"),
    NL("NETHERLANDS", "nl", "Nizozemska"),
    NO("NORWAY", "no", "Norveška"),
    PL("POLAND", "pl", "Poljska"),
    PT("PORTUGAL", "pt", "Portugalska"),
    RO("ROMANIA", "ro", "Romunija"),
    RU("RUSSIA", "ru", "Rusija"),
    MK("NORTH MACEDONIA", "mk", "Severna Makedonija"),
    SK("SLOVAKIA", "sk", "Slovaška"),
    SI("SLOVENIA", "si", "Slovenija"),
    ES("SPAIN", "es", "Španija"),
    RS("SERBIA", "rs", "Srbija"),
    SE("SWEDEN", "se", "Švedska"),
    CH("SWITZERLAND", "ch", "Švica"),
    TR("TURKEY", "tr", "Turčija"),
    UA("UKRAINE", "ua", "Ukrajina"),
    GB("UNITED KINGDOM", "gb", "V. Britanija"),
    XX("UNKNOWN", "unknown", "Neznano");

    public final String countryName;
    public final String fileFlagName;
    public final String websiteCountryName;
    private CountryEnum(String countryName, String fileFlagName, String websiteCountryName) {
        this.countryName = countryName;
        this.fileFlagName = fileFlagName;
        this.websiteCountryName = websiteCountryName;
    }

    public CountryEnum returnRightEnum(String websiteCountryName) {
        for (CountryEnum e : CountryEnum.values()) {
            if (e.websiteCountryName.equals(websiteCountryName)) {
                return e;
            }
        }
        return CountryEnum.XX;
    }
}
