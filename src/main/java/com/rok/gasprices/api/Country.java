package com.rok.gasprices.api;

import java.util.Date;

public class Country implements ICountry {

    private final CountryEnum country;
    private Double gasoline95;
    private Double gasoline98;
    private Double diesel;
    private String lastChange;


    public Country(CountryEnum country, Double gasoline95, Double gasoline98, Double diesel, String lastChange) {
        this.country = country;
        this.gasoline95 = gasoline95;
        this.gasoline98 = gasoline98;
        this.diesel = diesel;
        this.lastChange = lastChange;
    }

    @Override
    public CountryEnum getCountry() {
        return country;
    }

    @Override
    public Double getGasoline95() {
        return gasoline95;
    }

    @Override
    public Double getGasoline98() {
        return gasoline98;
    }

    @Override
    public Double getDiesel() {
        return diesel;
    }

    @Override
    public String getLastChange() {
        return lastChange;
    }

    @Override
    public void setGasoline95(Double price) {
        this.gasoline95 = price;
    }

    @Override
    public void setGasoline98(Double price) {
        this.gasoline98 = price;
    }

    @Override
    public void setDiesel(Double price) {
        this.diesel  = price;
    }

    @Override
    public void setLastChange(String lastChange) {
        this.lastChange = lastChange;
    }
}
