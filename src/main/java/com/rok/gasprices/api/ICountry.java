package com.rok.gasprices.api;

import java.util.Date;

public interface ICountry {

    CountryEnum getCountry();
    Double getGasoline95();
    Double getGasoline98();
    Double getDiesel();
    String getLastChange();
    void setGasoline95(Double price);
    void setGasoline98(Double price);
    void setDiesel(Double price);
    void setLastChange(String lastChange);

}
