package com.rok.gasprices.service.controllers;

import com.rok.gasprices.api.ICountry;
import com.rok.gasprices.service.components.GetLatestDataService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class HomePageController {

    @GetMapping("/")
    public String index() {

        List<ICountry> countries = GetLatestDataService.getData();

        String html = "<h1>Hello, World!</h1><table>";

        for (ICountry c : countries) {
            html += "<tr>" +
                        "<td>" + c.getCountry() + "</td>" +
                        "<td>" + c.getGasoline95() + "</td>" +
                        "<td>" + c.getGasoline98() + "</td>" +
                        "<td>" + c.getDiesel() + "</td>" +
                        "<td>" + c.getLastChange() + "</td>" +
                    "</tr>";
        }


        return html;
    }
}
