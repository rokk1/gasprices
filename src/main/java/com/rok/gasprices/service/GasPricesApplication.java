package com.rok.gasprices.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GasPricesApplication {

    public static void main(String[] args) {
        System.out.println("Starting application...");
        SpringApplication.run(GasPricesApplication.class, args);
    }
}
