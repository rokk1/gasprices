package com.rok.gasprices.service.components;

import com.rok.gasprices.api.Country;
import com.rok.gasprices.api.CountryEnum;
import com.rok.gasprices.api.ICountry;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Component
public class GetLatestDataService {

    private static List<ICountry> countries;

    private static String url;

    public GetLatestDataService() throws IOException {
        System.out.println("Starting getLatestDataService...");
        scrapeData();
    }

    private void scrapeData() throws IOException {
         url = "https://www.amzs.si/na-poti/cene-goriv-po-evropi";

        Document doc = Jsoup.connect(url).get();
        Elements table = doc.select("table");
        Elements tableBody = table.get(0).select("tbody");
        Elements tableRows = tableBody.get(0).select("tr");

        parseTableRows(tableRows);
    }

    private void parseTableRows(Elements tableRows) {

        countries = new ArrayList<>();

        for (Element row : tableRows) {
            Elements cols = row.select("td");
            /*  index
                  0 - flag & country name
                  1 - price gasoline95
                  2 - price gasoline98
                  3 - price diesel
                  4 - lastChange
             */

            Element countryName = cols.get(0);
            Element priceGasoline95 = cols.get(1);
            Element priceGasoline98 = cols.get(2);
            Element priceDiesel = cols.get(3);
            Element lastChange = cols.get(4);

            CountryEnum c = CountryEnum.AL.returnRightEnum(countryName.text());

            ICountry country = new Country(
                    c,
                    parsePrice(priceGasoline95.text()),
                    parsePrice(priceGasoline98.text()),
                    parsePrice(priceDiesel.text()),
                    lastChange.text().replace(" ", "")
            );

            countries.add(country);
        }

        countries.stream().forEach(country -> System.out.println(country.getCountry() + ": " + country.getGasoline95()));

        System.out.println("MAX PRICE: " + countries.stream().max(Comparator.comparing(ICountry::getGasoline95)).get().getCountry());
        System.out.println("MIN PRICE: " + countries.stream().min(Comparator.comparing(ICountry::getGasoline95)).get().getCountry());
    }

    private Double parsePrice(String price) {
        if (price.equals("")) {
            return null;
        }

        String[] priceFromBrackets = price.split("[\\(||\\)]");

        if (priceFromBrackets.length == 0) {
            return null;
        }

        String price2;

        if (priceFromBrackets.length == 1) {
            // No local current, only EUR
            price2 = priceFromBrackets[0].substring(0, priceFromBrackets[0].length() - 4).replace(",", ".");
        } else {
            price2 = priceFromBrackets[1].substring(0, priceFromBrackets[1].length() - 4).replace(",", ".");
        }

        return Double.parseDouble(price2);
    }

    public static List<ICountry> getData() {
        return countries;
    }

}
